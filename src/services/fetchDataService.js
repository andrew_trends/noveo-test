export default class dataFetchService {

  _apiURl = 'http://upload.noveogroup.com//files/32495/sample.json';
  _stripFields = ['shortDescription', 'title'];
  _dateOptions = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  };

  async fetchData() {
    const res = await fetch(this._apiURl);

    if(!res.ok) {
      console.log(res);
      throw new Error('Couldn\'t fetch data');
    }

    return await res.json();
  }

  getFeed = async () => {
    const { feed } = await this.fetchData();
    return feed.article.map(this._stripHtmlTags);
  };

  _stripHtmlTags = (data) => {
    if(!data) return null;

    const regex = /(<([^>]+)>)/ig;
    const stripedObject = {...data};
    this._stripFields.map(field => {
      if (!data[field]) return;
      return Object.assign(stripedObject, { [field]: data[field].replace(regex, '') });
    });
    if(data.date) {
      stripedObject.date = new Date(data.date).toLocaleString('en-US', this._dateOptions);
    }
    return stripedObject;
  }
}