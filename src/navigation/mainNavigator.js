import { createStackNavigator, createAppContainer } from 'react-navigation';

import HomeScreen from '../screens/homeScreen';
import ArticleDetailsScreen from '../screens/articleDetailsScreen';
import ArticleWebViewScreen from '../screens/articleWebView';

const stackNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: ({ navigation }) => ({
      title: `Articles List`,
      headerBackTitle: 'List',
    }),
  },
  ArticleDetails: {
    screen: ArticleDetailsScreen,
    navigationOptions: ({ navigation }) => {
      return {
        title: 'Details',
        headerBackTitle: 'Back',
      }
    },
  },
  ArticleLink: {
    screen: ArticleWebViewScreen,
  },
});

const appContainer = createAppContainer(stackNavigator);

export default appContainer;