import React, { Component } from 'react';
import {
  Modal,
  Button,
  Platform,
  View,
  TextInput,
  Text,
  Alert
} from 'react-native';

import { styles } from './sourceModal.styles';

class SourceModal extends Component {

  state = {
    modalVisible: false,
    urlValidation: true,
  };

  setModalVisible =() => {
    this.setState(prevState => {
      return { modalVisible: !prevState.modalVisible }
    });
  };

  onURLChange = (url) => {
    this.props.onUrlUpdate(url);
  };

  submitUrl = async () => {
    const { url, fetchFeedData } = this.props;
    const regex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/g;
    // check URL validity
    if(url.match(regex) === null) {
      this.setState({ urlValidation:  false });
      return;
    }
    if(url && url.length > 0) {
      fetchFeedData();
      this.setState({ urlValidation: true });
    }
    this.setModalVisible();
  };

  render() {
    const { url } = this.props;
    const { urlValidation } = this.state;

    return (
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.modalContainer}>
            <View>
              <TextInput
                placeholder="Enter URL"
                onChangeText={this.onURLChange}
                value={url}
                style={styles.urlInput}
                autoCorrect={false}
                underlineColorAndroid="transparent"
                onSubmitEditing={this.submitUrl}
                keyboardType={
                  Platform.select({
                    android: 'default',
                    ios: 'default'
                  })
                }
                clearButtonMode="while-editing"
                blurOnSubmit
                returnKeyType="done"
              />

              {
                !urlValidation && <Text>URL is not valid</Text>
              }

              <Button
                title="Ok"
                onPress={this.submitUrl}
              />
            </View>
          </View>
        </Modal>

        <Button
          title="Setup the source"
          onPress={this.setModalVisible}
        />
      </View>
    );
  }
}

export default SourceModal;