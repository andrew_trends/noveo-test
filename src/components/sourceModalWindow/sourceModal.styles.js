import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

const statusBarHeight = getStatusBarHeight();

export const styles = StyleSheet.create({
  modalContainer: {
    marginTop: statusBarHeight,
    flex: 1,
    paddingHorizontal: 20,
  },
  urlInput: {
    marginVertical: 20,
    paddingVertical: 5,
    paddingHorizontal: 10,
    fontSize: 16,
    borderBottomWidth: 1,
    borderBottomColor: 'steelblue',
  },
});