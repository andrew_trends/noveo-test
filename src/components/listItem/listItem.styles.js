import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
  },
  titleContainer: {
    flex: 4,
  },
  itemTitle: {
    fontSize: 20,
    color: '#000000',
    fontWeight: 'bold',
    marginBottom: 5,
  },
  itemDate: {
    fontSize: 14,
    color: '#777777',
    marginBottom: 15,
  },
  itemDescription: {
    fontSize: 18,
    color: '#464646',
  },
  imageStyles: {
    width: 60,
    height: 60,
    marginRight: 10,
    marginBottom: 10,
  }
});