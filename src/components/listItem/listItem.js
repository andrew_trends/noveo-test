import React, { PureComponent } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { ArticleImage } from '../index';
import { styles } from './listItem.styles';

class ListItem extends PureComponent {

  onPress = (item) => {
    this.props.onPressItem(item);
  };

  render() {
    const { item } = this.props;
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.onPress(item)}>
        <View>
          <View style={{
            flexDirection: 'row',
            flex: 1,
            alignItems: 'flex-start',
          }}>
            <ArticleImage
              url={item.imageUrl}
              imageStyles={styles.imageStyles}
              containerStyles={{ flex: 1 }}
            />
            <View style={styles.titleContainer}>
              <Text style={styles.itemTitle}>{item.title}</Text>
              <Text style={styles.itemDate}>{item.date}</Text>
            </View>
          </View>
          <Text style={styles.itemDescription}>{item.shortDescription}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default ListItem;