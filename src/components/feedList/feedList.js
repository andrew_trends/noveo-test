import React, {Component} from 'react';
import {
  View,
  FlatList,
  RefreshControl,
} from 'react-native';

import ListItem from '../listItem';
import { styles } from './feedList.styles';

class FeedList extends Component {

  renderItem = ({ item }) => {
    return <ListItem item={item} onPressItem={this.props.openArticle} />
  };

  keyExtractor = (item, index) => `${item.title}-${index}`;

  itemSeparator = () => <View style={styles.separator} />;

  render() {
    const {
      refreshing = false,
      onRefresh = () => {},
      list = {},
    } = this.props;

    return (
      <View>
        <FlatList
          data={list}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
          ItemSeparatorComponent={this.itemSeparator}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
            />
          }
        />
      </View>
    );
  }
}

export default FeedList;