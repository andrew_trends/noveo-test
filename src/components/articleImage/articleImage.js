import React, { PureComponent } from 'react';
import { Image, View } from 'react-native';

import { styles } from './articleImage.styles';

class ArticleImage extends PureComponent {
  render() {
    const {
      containerStyles = {},
      imageStyles = {},
      url = '',
      resizeMode = 'contain',
    } = this.props;
    return (
      <View styles={containerStyles}>
        <Image
          source={{ uri: url }}
          style={[styles.image, imageStyles]}
          resizeMode={resizeMode}
        />
      </View>
    );
  }
}

export default ArticleImage;