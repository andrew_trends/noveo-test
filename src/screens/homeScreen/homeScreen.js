import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
} from 'react-native';

import { SourceModal, FeedList } from '../../components';
import FetchDataService from '../../services/fetchDataService';
import { styles } from './homeScreen.styles';

class HomeScreen extends Component {

  fetchDataService = new FetchDataService();

  state = {
    refreshing: false,
    dataSource: '',
    articlesList: [],
    errorMessage: null,
  };

  openArticle = (article) => {
    const { navigate } = this.props.navigation;
    navigate('ArticleDetails', { article })
  };

  onUrlChange = (text) => {
    this.setState({ dataSource: text });
  };

  fetchFeedData = async () => {
    this.setState({ refreshing: true });
    const articles = await this.fetchDataService.getFeed()
      .catch(err => {
        this.setState({ errorMessage: err.message || 'Unknown error' })
      });
    this.setState({
      articlesList: articles,
      refreshing: false,
    });
  };

  render() {
    const {
      refreshing,
      dataSource,
      articlesList,
      errorMessage,
    } = this.state;

    return (
      <ScrollView>
        <View style={styles.screenContainer}>
          {
            dataSource.length === 0
            && <Text style={styles.emptySourceLabel}>The source is not defined</Text>
          }
          {
            errorMessage && <Text style={styles.error}>{errorMessage}</Text>
          }
          <FeedList
            list={articlesList}
            openArticle={this.openArticle}
            onRefresh={this.fetchFeedData}
            refreshing={refreshing}
          />
          <SourceModal
            url={dataSource}
            onUrlUpdate={this.onUrlChange}
            fetchFeedData={this.fetchFeedData}
          />
        </View>
      </ScrollView>
    );
  }
}

export default HomeScreen;