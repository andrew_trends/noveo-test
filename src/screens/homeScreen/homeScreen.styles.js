import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: '#ffffff',
    padding: 20,
  },
  emptySourceLabel: {
    fontSize: 20,
    color: 'steelblue',
    textAlign: 'center',
    marginVertical: 10,
  },
  error: {
    color: 'red',
    fontSize: 16,
    marginBottom: 20,
  },
});