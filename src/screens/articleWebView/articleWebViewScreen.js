import React, { Component } from 'react';
import { WebView } from 'react-native-webview';

class ArticleWebViewScreen extends Component {

  state = {
    articleLink: null,
  };

  componentDidMount() {
    const { getParam } = this.props.navigation;
    const articleLink = getParam('link', null);
    this.setState({ articleLink });
  }

  render() {
    const { articleLink } = this.state;

    return (
      <WebView
        source={{ uri: articleLink }}
        // style={{ marginTop: 20 }}
      />
    );
  }
}

export default ArticleWebViewScreen;