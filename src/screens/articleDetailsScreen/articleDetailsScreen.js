import React, { Component } from 'react';
import {
  Button,
  Dimensions,
  View,
  ScrollView,
  Text,
} from 'react-native';
import HTML from 'react-native-render-html';

import { ArticleImage } from '../../components';
import { styles } from './artcileDetails.styles';

class ArticleDetailsScreen extends Component {

  state = {
    article: {},
  };

  componentDidMount() {
    const { getParam } = this.props.navigation;
    this.setState({
      article: getParam('article', null),
    })
  }

  openInWebView = (link) => {
    this.props.navigation.navigate('ArticleLink', { link })
  };

  render() {
    const { article } = this.state;
    return (
      <ScrollView style={styles.container}>
        <View>
          <Text style={styles.postHeader}>{article.title}</Text>
          {
            Boolean(article.imageUrl)
            && <ArticleImage
              url={article.imageUrl}
              imageStyles={styles.articleImage}
              containerStyles={{ flex: 1 }}
            />
          }
          {
            article.description
            && <HTML
              html={article.description}
              imagesMaxWidth={Dimensions.get('window').width}
              baseFontStyle={styles.postBody}
            />
          }
          <Text style={styles.articleDate}>{article.date}</Text>
          <View style={styles.sourceButton}>
            <Button
              disabled={!article.link}
              title="Source link"
              onPress={() => this.openInWebView(article.link)}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default ArticleDetailsScreen;