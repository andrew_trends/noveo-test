import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
  },
  postHeader: {
    fontSize: 20,
    color: '#2e2e2e',
    fontWeight: 'bold',
    marginVertical: 15,
  },
  postBody: {
    fontSize: 18,
    color: '#464646',
    lineHeight: 28,
  },
  articleImage: {
    backgroundColor: 'black',
    width: '100%',
    height: 200,
    borderRadius: 5,
    marginBottom: 10,
  },
  articleDate: {
    color: '#2e2e2e',
    fontSize: 14,
    fontWeight: 'bold',
    marginVertical: 10,
  },
  sourceButton: {
    marginVertical: 20,
  },
});